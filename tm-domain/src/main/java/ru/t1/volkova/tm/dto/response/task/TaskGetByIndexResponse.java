package ru.t1.volkova.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
