package ru.t1.volkova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Task;

@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable final Task task) {
        super(task);
    }

}
