package ru.t1.volkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.task.*;
import ru.t1.volkova.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    String NAME = "TaskEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanse() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanse(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskGetByIndexResponse getTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIndexRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse getTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) throws SQLException;

}
