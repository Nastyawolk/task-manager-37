package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.IUserEndpoint;
import ru.t1.volkova.tm.dto.request.user.*;
import ru.t1.volkova.tm.dto.response.user.UserRegistryResponse;
import ru.t1.volkova.tm.marker.SoapCategory;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstanse();

    @NotNull
    private List<User> userList;

    private int userListSize;

    @Nullable
    private String token;

    @Before
    public void initTest() throws SQLException {
        userList = new ArrayList<>();

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserRegistryRequest request = new UserRegistryRequest(token);
            request.setLogin("userTest" + i);
            request.setEmail("userTest@" + i + ".ru");
            request.setPassword("userTest" + i + "pass");
            @NotNull final UserRegistryResponse responseCreate = userEndpoint.registryUser(request);
            userList.add(responseCreate.getUser());
        }

        userListSize = userList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangePassword() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("userTest1");
        requestLogin.setPassword("userTest1pass");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword("newPassword");
        userEndpoint.changeUserPassword(request);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        authEndpoint.logout(requestLogout);
        requestLogin.setPassword(request.getPassword());

        Assert.assertTrue(authEndpoint.login(requestLogin).getSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserLock() throws SQLException {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final String login = "userTest2";
        @NotNull final UserLockRequest request = new UserLockRequest(token);
        request.setLogin(login);
        @Nullable final User user = userEndpoint.lockUser(request).getUser();
        if (user == null) {return;}
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserRegistry() throws SQLException {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(token);
        @NotNull final String login = "userNew";
        @NotNull final String email = "userNew@ru";
        @NotNull final String password = "userNewpass";
        request.setLogin(login);
        request.setEmail(email);
        request.setPassword(password);
        @NotNull final UserRegistryResponse responseCreate = userEndpoint.registryUser(request);
        if (responseCreate.getUser() == null) {return;}
        Assert.assertEquals(login, responseCreate.getUser().getLogin());
        Assert.assertEquals(email, responseCreate.getUser().getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserRemove() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final String login = "userTest3";
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(token);
        request.setLogin(login);
        Assert.assertTrue(userEndpoint.removeUser(request).getSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserUnlock() throws SQLException {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final String login = "userTest2";
        @NotNull final UserLockRequest requestLock = new UserLockRequest(token);
        requestLock.setLogin(login);
        userEndpoint.lockUser(requestLock);

        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(token);
        requestUnlock.setLogin(login);
        @Nullable final User user = userEndpoint.unlockUser((requestUnlock)).getUser();
        if (user == null) {return;}
        Assert.assertFalse(user.getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserUpdateProfile() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("userTest4");
        requestLogin.setPassword("userTest4pass");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final String lastName = "newLastName";
        @NotNull final String firstName = "newFirstName";
        @NotNull final String middleName = "newMiddleName";
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setLastName(lastName);
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        @Nullable final User user = userEndpoint.updateUserProfile(request).getUser();
        if (user == null) {return;}
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

}
