package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.ProjectService;
import ru.t1.volkova.tm.service.PropertyService;
import ru.t1.volkova.tm.service.TaskService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectService projectService;

    @Before
    public void initRepository() throws SQLException {
        taskList = new ArrayList<>();
        taskService = new TaskService(connectionService);
        projectService = new ProjectService(connectionService);
        @NotNull final Project project = new Project();
        @NotNull final Project project2 = new Project();
        projectService.add(project);
        projectService.add(project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName("task" + i);
            task.setDescription("description" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId(project2.getId());
            }
            taskService.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testCreate(
    ) throws Exception {
        @Nullable final Task task = taskService.create(USER_ID_1, "new_task", "new description");
        if (task == null) return;
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize().intValue());
    }

    @Test
    public void testCreateNegative(
    ) throws SQLException {
        @Nullable final Task task = taskService.create(null, "new_task", "new description");
        Assert.assertNull(task);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) throws SQLException {
        taskService.create(USER_ID_1, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) throws SQLException {
        taskService.create(USER_ID_2, "new", null);
    }

    @Test
    public void testUpdateById() throws SQLException {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        @NotNull final String id = taskList.get(0).getId();
        taskService.updateById(USER_ID_1, id, newName, newDescription);
        @Nullable final Task task = taskService.findOneById(USER_ID_1, id);
        if (task == null) return;
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) throws SQLException {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        @NotNull final String id = "non-existent-id";
        taskService.updateById(USER_ID_1, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) throws SQLException {
        taskService.updateById(USER_ID_1, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) throws SQLException {
        taskService.updateById(USER_ID_1, taskList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) throws SQLException {
        taskService.updateById(USER_ID_2, taskList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() throws SQLException {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        taskService.updateByIndex(USER_ID_1, 2, newName, newDescription);
        @Nullable final Task task = taskService.findOneByIndex(USER_ID_1, 2);
        if (task == null) return;
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) throws SQLException {
        taskService.updateByIndex(USER_ID_1, 10, "name", "new description");
        taskService.updateByIndex(USER_ID_1, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) throws SQLException {
        taskService.updateByIndex(USER_ID_1, 2, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) throws SQLException {
        taskService.updateByIndex(USER_ID_2, 3, "name", null);
    }

    @Test
    public void testChangeStatusById() throws SQLException {
        @NotNull final String id = taskList.get(0).getId();
        @Nullable final Task task = taskService.changeTaskStatusById(USER_ID_1, id, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() throws SQLException {
        taskService.changeTaskStatusById(USER_ID_1, null, Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID_1, "", Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundById() throws SQLException {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID_1, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() throws SQLException {
        @Nullable final Task task = taskService.changeTaskStatusByIndex(USER_ID_1, 2, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() throws SQLException {
        taskService.changeTaskStatusByIndex(USER_ID_1, 22, Status.IN_PROGRESS);
        taskService.changeTaskStatusByIndex(USER_ID_1, null, Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundByIndex() throws SQLException {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID_1, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() throws SQLException {
        Assert.assertNotNull(taskService.findOneById(USER_ID_1, taskList.get(0).getId()));
        Assert.assertNotNull(taskService.findOneById(USER_ID_2, taskList.get(7).getId()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFindOneByIdNegative() throws SQLException {
        Assert.assertNotNull(taskService.findOneById(USER_ID_1, "non-existent"));
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(USER_ID_1, projectService.findOneByIndex(0).getId());
        @Nullable final List<Task> tasks2 = taskService.findAllByProjectId(USER_ID_2, projectService.findOneByIndex(1).getId());
        Assert.assertNotNull(tasks);
        Assert.assertNotNull(tasks2);
    }

    @Test
    public void findAllByProjectIdNegative() throws Exception {
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(null, projectService.findOneByIndex(0).getId());
        @NotNull final Project project = new Project();
        @Nullable final List<Task> tasks2 = taskService.findAllByProjectId(USER_ID_2, project.getId());
        Assert.assertNull(tasks);
        if (tasks2 == null) return;
        Assert.assertEquals(0, tasks2.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void findAllByProjectIdProjectNull() throws SQLException {
        taskService.findAllByProjectId(USER_ID_1, null);
        taskService.findAllByProjectId(USER_ID_1, "");
    }

}
