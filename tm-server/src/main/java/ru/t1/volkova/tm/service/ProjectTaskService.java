package ru.t1.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
            ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectRepository.findOneById(projectId) == null) throw new ProjectNotFoundException();
        @NotNull final Connection connection = taskRepository.getConnection();
        try {
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
            return task;
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) throws SQLException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        if (tasks == null || tasks.size() == 0) throw new TaskNotFoundException();
        try {
            for (@NotNull final Task task : tasks) {
                taskRepository.removeOneById(userId, task.getId());
                taskRepository.update(task);
                taskRepository.getConnection().commit();
            }
            @Nullable final Project project = projectRepository.removeOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.update(project);
            projectRepository.getConnection().commit();
        }
        catch (@NotNull final Exception e) {
            taskRepository.getConnection().rollback();
            projectRepository.getConnection().rollback();
            throw e;
        }
    }

    @Override
    @NotNull
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws SQLException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Connection connection = taskRepository.getConnection();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();

        try {
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return task;
    }

}
