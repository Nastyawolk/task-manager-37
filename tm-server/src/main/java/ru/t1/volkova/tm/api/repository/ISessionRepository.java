package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;

import java.sql.SQLException;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    void update(@NotNull Session session) throws SQLException;

}
