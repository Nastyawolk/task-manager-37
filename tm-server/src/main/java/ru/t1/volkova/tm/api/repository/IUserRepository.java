package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    void update(@NotNull User user) throws SQLException;

    @Nullable
    User findOneByLogin(@NotNull String login) throws SQLException;

    @Nullable
    User findOneByEmail(@NotNull String email) throws SQLException;

    @NotNull
    Boolean isLoginExist(@NotNull String login) throws SQLException;

    @NotNull
    Boolean isEmailExist(@NotNull String email) throws SQLException;

}
