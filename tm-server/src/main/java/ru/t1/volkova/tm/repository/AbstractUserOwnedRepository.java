package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.DBConstants;
import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import java.sql.*;
import java.util.*;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private final String USER_ID_COLUMN = DBConstants.COLUMN_USER_ID;

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id) throws SQLException {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws SQLException {
       @NotNull final List<M> result = new ArrayList<>();
       @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
       try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
       }
       return result;
    }

    @Nullable
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws SQLException {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        if (comparator != null) sql = String.format(sql + " ORDER BY %s", getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                resultSet.next();
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @Nullable final M model = findOneById(userId, id);
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.executeUpdate();
            return model;
        }
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @NotNull final Integer index) throws SQLException {
        @Nullable final M model = findOneByIndex(userId, index);
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getUserId());
            statement.executeUpdate();
            return model;
        }
    }

    @NotNull
    @Override
    public abstract M add(@Nullable final String userId, @NotNull final M model) throws SQLException;

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @NotNull final M model) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(1, userId);
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

}
