--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-09-07 20:32:25

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16425)
-- Name: tm_project; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_project (
    id text NOT NULL,
    name text,
    created date,
    description text,
    user_id text,
    status text
);


ALTER TABLE public.tm_project OWNER TO admin;

--
-- TOC entry 212 (class 1259 OID 16446)
-- Name: tm_session; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_session (
    id text NOT NULL,
    date date,
    user_id text,
    role text
);


ALTER TABLE public.tm_session OWNER TO admin;

--
-- TOC entry 210 (class 1259 OID 16430)
-- Name: tm_task; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_task (
    id text NOT NULL,
    name text,
    created date,
    description text,
    user_id text,
    status text,
    project_id text
);


ALTER TABLE public.tm_task OWNER TO admin;

--
-- TOC entry 211 (class 1259 OID 16435)
-- Name: tm_user; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_user (
    id text NOT NULL,
    login text,
    password text,
    email text,
    locked boolean,
    first_name text,
    last_name text,
    middle_name character varying,
    role text
);


ALTER TABLE public.tm_user OWNER TO admin;

--
-- TOC entry 3322 (class 0 OID 16425)
-- Dependencies: 209
-- Data for Name: tm_project; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('05108180-629b-456e-a416-d258a7f603ec', 'PROJECT12345', '2022-09-07', 'Project for TestUser', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'Not started');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('ce1ee606-4015-40af-953b-14b064830b1c', 'PROJECT12', '2022-09-07', 'Project 2 for TestUser', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'Not started');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('5c26297e-5baa-4b20-b05b-f9d276df59c3', 'PROJECT444', '2022-09-07', 'Project for CustomUser', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'Not started');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('b113b140-97b9-46f6-9126-f0d557afe39e', 'PROJECT123', '2022-09-07', 'Project for Admin', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('419b6e3d-1ead-44d8-ba80-3eaf440fb2fd', 'PROJECT1', '2022-09-07', 'Project 2 for Admin', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started');


--
-- TOC entry 3325 (class 0 OID 16446)
-- Dependencies: 212
-- Data for Name: tm_session; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('061c890f-2a04-4c13-a82a-5016d608ffd1', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('43ecee95-921c-4a27-a7ff-356560db22d1', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('f8cd6264-2f29-41a0-b024-9db96a3198bc', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('255c0037-3971-4f7b-ab38-9e57963bbb57', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('b754292b-8adf-47ec-b63a-27c0d1a4bf8c', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('b163fd51-99ca-4f6e-9a16-bd7009df9f50', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('204e4aa2-36e2-46f5-9114-f730122e5495', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('9ade48a4-ebf3-4140-8dd8-854ab128783b', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');


--
-- TOC entry 3323 (class 0 OID 16430)
-- Dependencies: 210
-- Data for Name: tm_task; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('2bac892a-6ac6-4148-bf32-ab5dad8b100e', 'TASK1234', '2022-09-07', 'test task', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'Not started', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('364e1ae7-4388-4c57-82f6-2d320557a3ec', 'TASK', '2022-09-07', 'test task', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'Not started', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('05fa8fcf-dbe0-46b6-beb4-213d2e0ecf36', 'new', '2022-09-07', 'nwe', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started', 'b113b140-97b9-46f6-9126-f0d557afe39e');
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('1d27cf50-e07e-4a6e-9c43-fc3c601c7f1a', 'TASK12', '2022-09-07', 'test task', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started', 'b113b140-97b9-46f6-9126-f0d557afe39e');
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('bb38df6c-0292-46d8-bd85-deb787ce0d35', 'TASK12', '2022-09-07', 'test task2', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'Not started', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('8658fe55-5e23-4ecc-b522-1ce9f288be0d', 'TASK5', '2022-09-07', 'test task', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'Not started', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('fc8edca1-3368-4cc8-994e-77559102b285', 'TASK1', '2022-09-07', 'test task', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('94e0f6f9-f528-4538-b014-8b81b3984baa', 'keke', '2022-09-07', 'ew', '488cc731-15ba-4e30-ac04-c58444d35671', 'Not started', NULL);


--
-- TOC entry 3324 (class 0 OID 16435)
-- Dependencies: 211
-- Data for Name: tm_user; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('df17407d-4764-4bd9-8cad-232f73e3f8b1', 'test', 'd969f78121bc32c1e5b889c533185a00', 'test@mail.ru', false, NULL, NULL, NULL, 'USUAL');
INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('37438da3-a264-45ae-9dda-29f8fa4817ee', 'user', 'e185e7f2ef1f3b8a18abae0f241c4b13', 'user@user.ru', false, NULL, NULL, NULL, 'USUAL');
INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('488cc731-15ba-4e30-ac04-c58444d35671', 'admin', '677eca73dc6474da8d041eedab5f1af8', NULL, false, NULL, NULL, NULL, 'ADMIN');


--
-- TOC entry 3176 (class 2606 OID 16454)
-- Name: tm_project tm_project_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_project
    ADD CONSTRAINT tm_project_pkey PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 16456)
-- Name: tm_session tm_session_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_session
    ADD CONSTRAINT tm_session_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 16458)
-- Name: tm_task tm_task_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_task
    ADD CONSTRAINT tm_task_pkey PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 16460)
-- Name: tm_user tm_user_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_user
    ADD CONSTRAINT tm_user_pkey PRIMARY KEY (id);


-- Completed on 2022-09-07 20:32:26

--
-- PostgreSQL database dump complete
--

